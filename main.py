"""A dummy docstring."""
from flask import Flask

app = Flask(__name__)


@app.route('/')
def index():
    """A dummy docstring."""
    return "Hello world !"


@app.route('/hello/<name>')
def hello_world(name):
    """A dummy docstring."""
    return str('Your slug is : ' + name)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

# base64 -d -> decode le mdp
# base64 < password