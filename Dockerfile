FROM python:3.9-alpine

WORKDIR /app

COPY . /app

RUN pip install -U Flask
RUN pip install -U pylint

CMD python main.py